package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexapi");
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index");
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		return view;
	}

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
		if (category.getId() != null) {
			Category newcategory = this.categoryRepository.findById(category.getId()).orElse(null);
			newcategory.setModifyBy("user1");
			newcategory.setModifyDate(new Date());
			newcategory.setCategoryInitial(category.getCategoryInitial());
			newcategory.setCategoryName(category.getCategoryName());
			newcategory.setIsActive(category.getIsActive());
			category = newcategory;
			this.categoryRepository.save(category);
		} else if (!result.hasErrors()) {
			category.setCreatedBy("user1"); // Setting user default user1
			category.setCreatedDate(new Date()); // now() Setting user default waktu saat save
			this.categoryRepository.save(category); // INSERT INTO
		}

		return new ModelAndView("redirect:/category/index");

	}

	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = this.categoryRepository.findById(id).orElse(null); // SELECT * FROM category WHERE id = {id}
		view.addObject("category", category);
		return view;
	}

	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids") Long id) {
		if (id != null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}

}
