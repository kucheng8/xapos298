package com.xapos298.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Product;
import com.xapos298.demo.repository.ProductRepository;
import com.xapos298.demo.repository.VariantRepository;

@Controller
@RequestMapping("/product/")
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantRepository variantRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("product/indexapi");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listProduct", listProduct);
		return view;
	}

}
