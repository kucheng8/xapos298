package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variant;
import com.xapos298.demo.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantController {

	@Autowired
	public VariantRepository variantRepository;

	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant() {
		try {
			List<Variant> listVariant = this.variantRepository.findByIsActiveAndSortByVariantName(true);
			return new ResponseEntity<List<Variant>>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("variant/add")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {

		variant.setCreatedBy("user1");
		variant.setCreatedDate(new Date());
		Variant variantData = this.variantRepository.save(variant);

		if (variantData.equals(variant)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("variant/{id}")
	public ResponseEntity<List<Variant>> getVariantById(@PathVariable("id") Long id) {
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			if (variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/variant/{id}")
	public ResponseEntity<Object> editVariant(@PathVariable("id") Long id, @RequestBody Variant variant) {

		Optional<Variant> variantData = this.variantRepository.findById(id);
		if (variantData.isPresent()) {
			variant.setId(variantData.get().getId());
			variant.setModifyBy("user1");
			variant.setModifyDate(new Date());
			variant.setCreatedBy(variantData.get().getCreatedBy());
			variant.setCreatedDate(variantData.get().getCreatedDate());
			this.variantRepository.save(variant);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Update Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("delete/variant/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {

		Optional<Variant> variantData = this.variantRepository.findById(id);

		if (variantData.isPresent()) {

			Variant variant = new Variant();
			variant.setId(id);
			variant.setIsActive(false);
			variant.setModifyBy("user1");
			variant.setModifyDate(new Date());
			variant.setCreatedBy(variantData.get().getCreatedBy());
			variant.setCreatedDate(variantData.get().getCreatedDate());
			variant.setVariantInitial(variantData.get().getVariantInitial());
			variant.setVariantName(variantData.get().getVariantName());
			variant.setCategoryId(variantData.get().getCategoryId());
			this.variantRepository.save(variant);
			return new ResponseEntity<Object>("Deleted Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Update Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("variantbycategory/{id}")
	public ResponseEntity<List<Variant>> getVariantByCategoryId(@PathVariable("id") Long id) {
		try {
			List<Variant> variant = this.variantRepository.findByVarCatId(true, true, id);
			return new ResponseEntity<List<Variant>>(variant, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	@GetMapping("variantsearch/{keyword}")
	public ResponseEntity<List<Variant>> getSearchVariant(@PathVariable("keyword") String keyword) {
		try {
			List<Variant> listVariant = this.variantRepository.findVariantNameByKeyword(keyword);
			return new ResponseEntity<List<Variant>>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
