package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variant;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.VariantRepository;

@Controller
@RequestMapping("/variant/")
public class VariantController {
	
	@Autowired
	private VariantRepository variantRepository;

	@Autowired
	private CategoryRepository categoryRepository; // Import repository category
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("variant/indexapi");
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("listVariant", listVariant);
		return view;
	}
	
	@GetMapping("index") 
	public ModelAndView index(){
		ModelAndView view = new ModelAndView("variant/index");
		List<Variant> listVariant = this.variantRepository.findByIsActive(true);
		view.addObject("listVariant", listVariant);
		return view;
	}
	
	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("variant/addform");
		Variant variant = new Variant();
		view.addObject("variant", variant);
		List<Category> listCategory = this.categoryRepository.findAll(); // Mengambil List Category
		view.addObject("listCategory", listCategory);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variant variant, BindingResult result) {
		if (variant.getId() != null) {
			Variant newvariant = this.variantRepository.findById(variant.getId()).orElse(null);
			newvariant.setModifyBy("user1");
			newvariant.setModifyDate(new Date());
			newvariant.setCategoryId(variant.getCategoryId());
			newvariant.setVariantInitial(variant.getVariantInitial());
			newvariant.setVariantName(variant.getVariantName());
			newvariant.setIsActive(variant.getIsActive());
			variant = newvariant;
			this.variantRepository.save(variant);
		} else if (!result.hasErrors()) {
			variant.setCreatedBy("user1");
			variant.setCreatedDate(new Date());
			this.variantRepository.save(variant);
		}

		return new ModelAndView("redirect:/variant/index");
	}
	
	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) {
		ModelAndView view = new ModelAndView("variant/addform");
		Variant variant = this.variantRepository.findById(id).orElse(null);
		view.addObject("variant", variant);
		List<Category> listCategory = this.categoryRepository.findAll();  // tambahkan list category
		view.addObject("listCategory", listCategory);
		return view;
	}
	
	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids") Long id) {
		if (id != null) {
			this.variantRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/variant/index");
	}
	
}
