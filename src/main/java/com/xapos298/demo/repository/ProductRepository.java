package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Product;
import com.xapos298.demo.model.Variant;

public interface ProductRepository extends JpaRepository<Product, Long> {
	List<Product> findByisActive(Boolean isActive);
	
	@Query(value = "SELECT * FROM product WHERE is_active = ? ORDER BY id", nativeQuery = true)
	List<Product> findByisActiveAndSortById(Boolean apaAktif);
	
	@Query(value = "SELECT * FROM product WHERE lower(product_name) LIKE lower(concat('%', ? ,'%')) AND is_active = true", nativeQuery = true)
	List<Product> findProductNameByKeyword(String keyword);

}
