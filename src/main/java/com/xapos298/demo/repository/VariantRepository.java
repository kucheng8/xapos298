package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {
	
	List<Variant> findByVariantInitial(String variantInitial);

	List<Variant> findByIsActive(Boolean isActive);

	List<Variant> findByCreatedBy(String createdBy);

	@Query(value = "SELECT * FROM variant WHERE is_active = ?1 AND created_by = ?2", nativeQuery = true)
	List<Variant> findByIsActiveAndCreatedByManual(Boolean apaAktif, String createdBy); // Manual

	List<Variant> findByIsActiveAndCreatedBy(Boolean apaAktif, String createdBy); // Suggest java
	
	List<Variant> findByCategoryId(Long id);
	
	@Query(value = "SELECT * FROM variant WHERE is_active = ? ORDER BY id", nativeQuery = true)
	List<Variant> findByIsActiveAndSortById(Boolean apaAktif);
	
	@Query(value = "SELECT * FROM variant WHERE is_active = ? ORDER BY variant_name", nativeQuery = true)
	List<Variant> findByIsActiveAndSortByVariantName(Boolean apaAktif);
	
	@Query(value = "SELECT * FROM variant v JOIN category c ON v.category_id = c.id WHERE v.is_active = ?1 AND c.is_active = ?2 AND c.id = ?3", nativeQuery = true)
	List<Variant> findByVarCatId(Boolean apaAktif1, Boolean apaAktif2, Long id);
	
	@Query(value = "SELECT * FROM variant  WHERE lower(variant_name) LIKE lower(concat('%', ? ,'%')) AND is_active = true", nativeQuery = true)
	List<Variant> findVariantNameByKeyword(String keyword);

}
