package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.OrderHeader;
import com.xapos298.demo.model.Variant;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	List<Category> findByIsActive(Boolean isActive);
	
	@Query(value = "SELECT * FROM category WHERE is_active = ? ORDER BY id", nativeQuery = true)
	List<Category> findByIsActiveAndSortById(Boolean apaAktif);
	
	@Query(value = "SELECT * FROM category WHERE is_active = ? ORDER BY category_name", nativeQuery = true)
	List<Category> findByIsActiveAndSortByCategoryName(Boolean apaAktif);
	
	@Query(value = "SELECT * FROM category WHERE lower(category_name) LIKE lower(concat('%', ? ,'%')) AND is_active = true", nativeQuery = true)
	List<Category> findCategoryNameByKeyword(String keyword);

}
