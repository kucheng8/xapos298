$(function() {
	getAllVariant()
})
function getAllVariant() {
	$.ajax({
		url: '/api/variant',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var isiHome = "<table border='1' class='table bg-light'>"
			isiHome += "<thead>"
			isiHome += "<tr>"
			isiHome += "<th class='text-center'>Category</th>"
			isiHome += "<th class='text-center'>Initial</th>"
			isiHome += "<th class='text-center'>Name</th>"
			isiHome += "<th class='text-center'>Active</th>"
			isiHome += "<th colspan='3'  class='text-center'>Action</th>"
			isiHome += "</tr>"
			isiHome += "</thead>"
			isiHome += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				isiHome += "<tr>"
				isiHome += "<td>" + data[i].category.categoryName + "</td>"
				isiHome += "<td>" + data[i].variantInitial + "</td>"
				isiHome += "<td>" + data[i].variantName + "</td>"
				isiHome += "<td class='text-center'><input type='checkbox' name='isactive' checked disabled ></td>"
				isiHome += "<td class='text-center'><button class='btn btn-warning' onclick='openModalEdit(" + data[i].id + ") '><i class='bi-pencil-square'></button></td>"
				isiHome += "<td class='text-right'><button class='btn btn-danger' onclick='openModalDelete(" + data[i].id + ") '><i class='bi-trash'></button></td>"
				isiHome += "<td class='text-center'><input type='checkbox' id='willBeDeleted'></td>"
				isiHome += "</tr>"

			}
			isiHome += "</tbody>"
			isiHome += "</table>"

			$('#isidata').html(isiHome)
		}
	})
}

function openModal() {
	$.ajax({
		url: '/api/category',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var isiModalAwal = "<table>"
			isiModalAwal += "<input type='hidden' id='variantId' value =0>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td width=75px><b>Category</b></td>"
			isiModalAwal += "<td><select id='categoryId'>"
			for (var i = 0; i < data.length; i++) {
				isiModalAwal += "<option value='" + data[i].id + "'>" + data[i].categoryName + "</option>"
			}
			isiModalAwal += "</select></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td width=75px><b>Initial</b></td>"
			isiModalAwal += "<td><input type='text' id='variantInitial'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td><b>Name</b></td>"
			isiModalAwal += "<td><input type='text' id='variantName'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td><b>Active</b></td>"
			isiModalAwal += "<td><input type='checkbox' id='isActive'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "</table>"

			$('.modal-title').html('Create New')
			$('.modal-body').html(isiModalAwal)
			$('#btn-save').off('click').on('click', saveVariant).html('Create')
			$('#modal').modal('show')
		}
	})

}

function saveVariant() {
	var id = $('#variantId').val()
	var categoryId = $('#categoryId').val()
	var initial = $('#variantInitial').val()
	var name = $('#variantName').val()
	var active = $('#isActive')[0].checked

	var dataSave = '{'
	dataSave += '"categoryId" : "' + categoryId + '",'
	dataSave += '"variantInitial" : "' + initial + '",'
	dataSave += '"variantName" : "' + name + '",'
	dataSave += '"isActive" : ' + active + ''
	dataSave += '}'

	var link = ''
	var metode = ''
	if (id == 0) {
		link = '/api/variant/add'
		metode = 'post'
	} else {
		link = '/api/edit/variant/' + id + ''
		metode = 'put'
	}
	$.ajax({
		url: link,
		type: metode,
		contentType: 'application/json',
		data: dataSave,
		success: function() {
			$('#modal').modal('toggle')
			getAllVariant()
		}
	})
}

function openModalEdit(id2) {
	openModal()
	$.ajax({
		url: '/api/variant/' + id2,
		type: 'get',
		contentType: 'application/json',
		success: function(data2) {
			$('#categoryId').val(data2.categoryId);
			$('#variantId').val(data2.id);
			$('#variantInitial').val(data2.variantInitial);
			$('#variantName').val(data2.variantName);
			$('#isActive').prop('checked', true);
			$('.modal-title').html('Edit');
			$('#btn-save').off('click').on('click', saveVariant).html('Edit');

		}
	})
}

function openModalDelete(id3) {
	openModal()
	$.ajax({
		url: '/api/variant/' + id3,
		type: 'get',
		contentType: 'application/json',
		success: function(data2) {
			$('#categoryId').val(data2.categoryId).prop('disabled', true);
			$('#variantId').val(data2.id);
			$('#variantInitial').val(data2.variantInitial).prop('disabled', true);
			$('#variantName').val(data2.variantName).prop('disabled', true);
			$('#isActive').prop('checked', true).prop('disabled', true);
			$('.modal-title').html('Delete');
			$('#btn-save').html('Delete').off('click').on('click', deleteVariant);

		}
	})
}

function deleteVariant() {
	var id = $('#variantId').val()

	$.ajax({
		url: '/api/delete/variant/' + id + '',
		type: 'put',
		contentType: 'application/json',
		data: id,
		success: function() {
			$('#modal').modal('toggle')
			getAllVariant()
		}
	})
}

function search() {
	$.ajax({
		url: '/api/variantsearch/' + $('#input-search').val(),
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var isiHome = "<table border='1' class='table bg-light'>"
			isiHome += "<thead>"
			isiHome += "<tr>"
			isiHome += "<th class='text-center'>Category</th>"
			isiHome += "<th class='text-center'>Initial</th>"
			isiHome += "<th class='text-center'>Name</th>"
			isiHome += "<th class='text-center'>Active</th>"
			isiHome += "<th colspan='2'  class='text-center'>Action</th>"
			isiHome += "</tr>"
			isiHome += "</thead>"
			isiHome += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				isiHome += "<tr>"
				isiHome += "<td>" + data[i].category.categoryName + "</td>"
				isiHome += "<td>" + data[i].variantInitial + "</td>"
				isiHome += "<td>" + data[i].variantName + "</td>"
				isiHome += "<td class='text-center'><input type='checkbox' name='isactive' checked disabled ></td>"
				isiHome += "<td class='text-right'><button class='btn btn-warning' onclick='openModalEdit(" + data[i].id + ") '><i class='bi-pencil-square'></button></td>"
				isiHome += "<td class='text-left'><button class='btn btn-danger' onclick='openModalDelete(" + data[i].id + ") '><i class='bi-trash'></button></td>"
				isiHome += "</tr>"

			}
			isiHome += "</tbody>"
			isiHome += "</table>"

			$('#isidata').html(isiHome)
		}
	})
}