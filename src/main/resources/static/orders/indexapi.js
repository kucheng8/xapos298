var tempStock = 0;

function allTrans() {
	$.ajax({
		url: '/api/orderheader',
		type: 'get',
		contenType: 'application/json',
		success: function(data) {
			var isiAwal = "<table class='table'>"
			isiAwal += "<thead>"
			isiAwal += "<tr>"
			isiAwal += "<th>No</th>"
			isiAwal += "<th class='text-center'>Reference</th>"
			isiAwal += "<th class='text-right'>Amount</th>"
			isiAwal += "<th class='text-center'>Delete</th>"
			isiAwal += "</tr>"
			isiAwal += "</thead>"
			isiAwal += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				isiAwal += "<tr>"
				isiAwal += "<td>" + (i + 1) + "</td>"
				isiAwal += "<td class='text-center'>" + data[i].reference + "</td>"
				isiAwal += "<td class='text-right'>" + data[i].amount + "</td>"
				isiAwal += "<td class='text-center'><input type='checkbox' id='willBeDeleted'></td>"
				isiAwal += "</tr>"
			}
			isiAwal += "</tbody>"
			isiAwal += "</table>"

			$('#main-contain').html(isiAwal)
			$("#isidata").html("")



		}
	})
}


function buka() {

	var buatRef = '{"reference": "", "amount": ""}'
	$.ajax({
		url: '/api/orderheader/add',
		type: 'post',
		contentType: 'application/json',
		data: buatRef,
		success: function() {
			cariHeaderId()
		}
	})
}

function cariHeaderId() {
	$.ajax({
		url: '/api/maxorderheaderid',
		type: 'get',
		contentType: 'application/json',
		success: function(headerId) {
			newOrder(headerId)
		}
	})
}

function newOrder(headerId) {
	$.ajax({
		url: '/api/product',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var isiNewOrder = "<table class='table'>"
			isiNewOrder += "<tr>"
			isiNewOrder += "<td width=300px ><b>Product</b></td>"
			isiNewOrder += "<td><select id='product_id' onchange='hargaProduk(this.value)'>"
			isiNewOrder += "<option value=0 >--   Select Product   --</option>"
			for (var i = 0; i < data.length; i++) {
				isiNewOrder += "<option value=" + data[i].id + ">" + data[i].productName + "</option>"
			}
			isiNewOrder += "</select></td></tr>"
			isiNewOrder += "<tr><td><b>Price</b></td>"
			isiNewOrder += "<td><input type='text' id='product_price' disabled></td>"
			isiNewOrder += "</tr>"
			isiNewOrder += "<tr>"
			isiNewOrder += "<td width=75px><b>Quantity</b></td>"
			isiNewOrder += "<td><input type='number' onkeyup='validateStock()' id='quantity'></td>"
			isiNewOrder += "</tr>"
			isiNewOrder += "<tr>"
			isiNewOrder += "<td><button class='btn btn-success' onclick='addOrder(" + headerId + ")' >Tambah item</button></td>"
			isiNewOrder += "</tr>"
			isiNewOrder += "</table>"

			$('#main-contain').html(isiNewOrder)
		}
	})

}

function showOrder(headerId) {
	$.ajax({
		url: '/api/orderdetail/' + headerId,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var totalAmount = 0;
			var str = "<table class = 'table'>"
			str += "<tr>"
			str += "<td>Reference</td>"
			str += "<td>Product</td>"
			str += "<td>Price</td>"
			str += "<td>Quantity</td>"
			str += "<td>Amount</td>"
			str += "</tr>"
			for (var i = 0; i < data.length; i++) {
				totalAmount += (data[i].quantity * data[i].product.productPrice)
				str += "<tr>"
				str += "<td>" + data[i].orderHeader.reference + "</td>"
				str += "<td>" + data[i].product.productName + "</td>"
				str += "<td>" + data[i].product.productPrice + "</td>"
				str += "<td>" + data[i].quantity + "</td>"
				str += "<td>" + data[i].quantity * data[i].product.productPrice + "</td>"
				str += "</tr>"
			}
			str += "<tr><td colspan = '4'>Total Amount</td>"
			str += "<td><input id = 'totalAmount' value=" + totalAmount + " disabled></td>"
			str += "</tr>"
			str += "<tr>"
			str += "<td><button class='btn btn-success' onclick='confirmModal(" + headerId + ")'>Checkout</button></td>"
			str += "</tr>"
			str += "</table>"
			$('#data-list').html(str)
		}
	})
}


function hargaProduk(id) {
	if (id > 0) {
		$.ajax({
			url: '/api/product/' + id,
			type: 'get',
			contentType: 'application/json',
			success: function(data) {
				$('#product_price').val(data.productPrice)
				$('#quantity').attr("max", data.productStock)
				$('#quantity').attr("min", 1)
				tempStock = data.productStock;
			}
		})
	} else {
		$('#product_price').val(0)
	}

}

function validateStock() {
	if ($('#quantity').val() > tempStock) {
		$('#quantity').val(tempStock)
	}
}

function addOrder(headerId) {
	var price = $('#product_price').val();
	var productId = $('#product_id').val();
	var qty = $('#quantity').val();

	var str = '{'
	str += '"headerId" : ' + headerId + ','
	str += '"productId" : ' + productId + ','
	str += '"quantity" : ' + qty + ','
	str += '"price" : ' + price + ''
	str += '}'

	$.ajax({
		url: '/api/orderdetail/add',
		type: 'post',
		contentType: 'application/json',
		data: str,
		success: function() {
			$('#product_price').val(0)
			$('#product_id').val(0)
			$('#quantity').val(1)
			newOrder(headerId)
			showOrder(headerId)
		}
	})

}

function confirmModal(headerId) {
	$.ajax({
		url: '/api/orderdetail/' + headerId,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = "<h5>Apakah anda yakin ingin checkout barang ini?</h5>"
			str += "<table class ='table table-border-borderless'>"
			str += "<thead>"
			str += "<tr>"
			str += "<th>No</th>"
			str += "<th>Product Name</th>"
			str += "<th>Quantity</th>"
			str += "</tr>"
			str += "</thead>"
			str += "<tbody>"
			var ref = 0
			for (var i = 0; i < data.length; i++) {
				str += "<tr>"
				str += "<td>" + (i + 1) + "</td>"
				str += "<td>" + data[i].product.productName + "</td>"
				str += "<td>" + data[i].quantity + "</td>"
				str += "</tr>"

				ref = data[i].orderHeader.reference
			}
			str += "</tbody>"
			str += "</table>"
			$('.modal-body').html(str)
			$('#modal').modal('show')
			$('#btn-save').off('click').on('click', function() { doneProcess(headerId, ref) }).html('Checkout')
		}
	})
}

function doneProcess(headerId, ref) {
	console.log(ref)
	console.log(headerId)
	var formdata = '{'
	formdata += '"id": ' + headerId + ','
	formdata += '"amount": "' + $('#totalAmount').val() + '",'
	formdata += '"reference": "' + ref + '"'
	formdata += '}'

	$.ajax({
		url: '/api/orderheader/done',
		type: 'put',
		contentType: 'application/json',
		data: formdata,
		success: function() {
			location.href = 'indexapi'
		}
	})
}


function search() {
	$.ajax({
		url: '/api/orderheadersearch/' + $('#input-search').val(),
		type: 'get',
		contenType: 'application/json',
		success: function(data) {
			var isiAwal = "<table class='table'>"
			isiAwal += "<thead>"
			isiAwal += "<tr>"
			isiAwal += "<th>No</th>"
			isiAwal += "<th>Reference</th>"
			isiAwal += "<th>Amount</th>"
			isiAwal += "</tr>"
			isiAwal += "</thead>"
			isiAwal += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				isiAwal += "<tr>"
				isiAwal += "<td>" + (i + 1) + "</td>"
				isiAwal += "<td>" + data[i].reference + "</td>"
				isiAwal += "<td>" + data[i].amount + "</td>"
				isiAwal += "</tr>"
			}
			isiAwal += "</tbody>"
			isiAwal += "</table>"

			$('#main-contain').html(isiAwal)
			$("#isidata").html("")



		}
	})
}