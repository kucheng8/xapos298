$(function() {
	getAllProduct()
})
function getAllProduct() {
	$.ajax({
		url: '/api/product',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var isiHome = "<table border='1' class='table bg-light'>"
			isiHome += "<thead>"
			isiHome += "<tr>"
			isiHome += "<th class='text-center'>Category/Variant</th>"
			isiHome += "<th class='text-center'>Initial</th>"
			isiHome += "<th class='text-center'>Name</th>"
			isiHome += "<th class='text-center'>Description</th>"
			isiHome += "<th class='text-center'>Price</th>"
			isiHome += "<th class='text-center'>Stock</th>"
			isiHome += "<th class='text-center'>Active</th>"
			isiHome += "<th colspan='2'  class='text-center'>Action</th>"
			isiHome += "</tr>"
			isiHome += "</thead>"
			isiHome += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				isiHome += "<tr>"
				isiHome += "<td>" + data[i].variant.category.categoryName + " / " + data[i].variant.variantName + "</td>"
				isiHome += "<td>" + data[i].productInitial + "</td>"
				isiHome += "<td>" + data[i].productName + "</td>"
				isiHome += "<td>" + data[i].productDescription + "</td>"
				isiHome += "<td>" + data[i].productPrice + "</td>"
				isiHome += "<td>" + data[i].productStock + "</td>"
				isiHome += "<td class='text-center'><input type='checkbox' name='isactive' checked disabled></td>"
				isiHome += "<td class='text-right><button class='btn btn-warning' onclick='openModalEdit(" + data[i].id + ")'><i class='bi-pencil-square'></td>"
				isiHome += "<td class='text-right><button class='btn btn-danger' onclick='openModalDelete(" + data[i].id + ")'><i class='bi-trash'></td>"
				isiHome += "</tr>"
			}
			isiHome += "</tbody>"
			isiHome += "<table>"

			$('#isidata').html(isiHome)
		}
	})
}

function openModal() {

	$.ajax({
		url: '/api/category',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var isiModalAwal = "<table>"
			isiModalAwal += "<input type='hidden' id='productId' value =0>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td width=75px><b>Category</b></td>"
			isiModalAwal += "<td><select id='categoryId' onchange='VariantByCategoryId(this.value)'>"
			isiModalAwal += "<option>-- Select Category --</option>"
			for (var i = 0; i < data.length; i++) {
				isiModalAwal += "<option value=" + data[i].id + ">" + data[i].categoryName + "</option>"
			}
			isiModalAwal += "</select></td></tr>"
			isiModalAwal += "<tr><td><b>Variant</b></td>"
			isiModalAwal += "<td><select id='variantId'>"
			isiModalAwal += "</select></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td width=75px><b>Initial</b></td>"
			isiModalAwal += "<td><input type='text' id='productInitial'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td><b>Name</b></td>"
			isiModalAwal += "<td><input type='text' id='productName'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td><b>Description</b></td>"
			isiModalAwal += "<td><input type='text' id='productDescription'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td><b>Price</b></td>"
			isiModalAwal += "<td><input type='number' id='productPrice'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td><b>Stock</b></td>"
			isiModalAwal += "<td><input type='number' id='productStock'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "<tr>"
			isiModalAwal += "<td><b>Active</b></td>"
			isiModalAwal += "<td><input type='checkbox' id='isActive'></td>"
			isiModalAwal += "</tr>"
			isiModalAwal += "</table>"

			$('.modal-title').html('Create New')
			$('.modal-body').html(isiModalAwal)
			$('#btn-save').off('click').on('click', saveProduct).html('Create')
			$('#modal').modal('show')
		}
	})

}

function VariantByCategoryId(categoryId, variantId) {
	$.ajax({
		url: '/api/variantbycategory/' + categoryId + '',
		type: 'get',
		contentType: 'application/json',
		success: function(dataVar) {
			var str = "<option>-- Select Variant --</option>";
			for (i = 0; i < dataVar.length; i++) {
				if (dataVar[i].id == variantId) {
					select = "selected";
				} else {
					select = "";
				}
				str += '<option  value = "' + dataVar[i].id + '" ' + select + '>' + dataVar[i].variantName + '</option>';
			}
			$('#variantId').html(str);

		}
	})
}

function saveProduct() {
	var id = $('#productId').val()
	var variantId = $('#variantId').val()
	var initial = $('#productInitial').val()
	var name = $('#productName').val()
	var description = $('#productDescription').val()
	var price = $('#productPrice').val()
	var stock = $('#productStock').val()
	var active = $('#isActive')[0].checked

	var dataSave = '{'
	dataSave += '"productInitial" : "' + initial + '",'
	dataSave += '"productName" : "' + name + '",'
	dataSave += '"productDescription" : "' + description + '",'
	dataSave += '"productPrice" : "' + price + '",'
	dataSave += '"productStock" : "' + stock + '",'
	dataSave += '"isActive" : "' + active + '",'
	dataSave += '"variantId" : ' + variantId + ''
	dataSave += '}'

	var link = ''
	var metode = ''
	if (id == 0) {
		link = '/api/product/add'
		metode = 'post'
	} else {
		link = '/api/edit/product/' + id + ''
		metode = 'put'
	}
	$.ajax({
		url: link,
		type: metode,
		contentType: 'application/json',
		data: dataSave,
		success: function() {
			$('#modal').modal('toggle')
			getAllProduct()
		}
	})
}

function openModalEdit(id2) {
	openModal()
	$.ajax({
		url: '/api/product/' + id2,
		type: 'get',
		contentType: 'application/json',
		success: function(data2) {

			$('#categoryId').val(data2.variant.category.id);
			VariantByCategoryId(data2.variant.category.id, data2.variantId);
			$('#productId').val(data2.id);
			$('#productInitial').val(data2.productInitial);
			$('#productName').val(data2.productName);
			$('#productDescription').val(data2.productDescription);
			$('#productPrice').val(data2.productPrice);
			$('#productStock').val(data2.productStock);
			$('#isActive').prop('checked', true);
			$('.modal-title').html('Edit');
			$('#btn-save').off('click').on('click', saveProduct).html('Edit');

		}
	})
}


function openModalDelete(id3) {
	openModal()
	$.ajax({
		url: '/api/product/' + id3,
		type: 'get',
		contentType: 'application/json',
		success: function(data2) {
			$('#categoryId').val(data2.variant.category.id).prop('disabled', true);
			VariantByCategoryId(data2.variant.category.id, data2.variantId);
			$('#variantId').val(data2.variantId).prop('disabled', true);
			$('#productId').val(data2.id).prop('disabled', true);
			$('#productInitial').val(data2.productInitial).prop('disabled', true);
			$('#productName').val(data2.productName).prop('disabled', true);
			$('#productDescription').val(data2.productDescription).prop('disabled', true);
			$('#productPrice').val(data2.productPrice).prop('disabled', true);
			$('#productStock').val(data2.productStock).prop('disabled', true);
			$('#isActive').prop('checked', true).prop('disabled', true);
			$('.modal-title').html('Delete');
			$('#btn-save').html('Delete').off('click').on('click', deleteVariant);

		}
	})
}

function deleteVariant() {
	var id = $('#productId').val()

	$.ajax({
		url: '/api/delete/product/' + id + '',
		type: 'put',
		contentType: 'application/json',
		data: id,
		success: function() {
			$('#modal').modal('toggle')
			getAllProduct()
		}
	})
}

function search() {
	$.ajax({
		url: '/api/productsearch/' + $('#input-search').val(),
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var isiHome = "<table border='1' class='table bg-light'>"
			isiHome += "<thead>"
			isiHome += "<tr>"
			isiHome += "<th class='text-center'>Category/Variant</th>"
			isiHome += "<th class='text-center'>Initial</th>"
			isiHome += "<th class='text-center'>Name</th>"
			isiHome += "<th class='text-center'>Description</th>"
			isiHome += "<th class='text-center'>Price</th>"
			isiHome += "<th class='text-center'>Stock</th>"
			isiHome += "<th class='text-center'>Active</th>"
			isiHome += "<th colspan='2'  class='text-center'>Action</th>"
			isiHome += "</tr>"
			isiHome += "</thead>"
			isiHome += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				isiHome += "<tr>"
				isiHome += "<td>" + data[i].variant.category.categoryName + " / " + data[i].variant.variantName + "</td>"
				isiHome += "<td>" + data[i].productInitial + "</td>"
				isiHome += "<td>" + data[i].productName + "</td>"
				isiHome += "<td>" + data[i].productDescription + "</td>"
				isiHome += "<td>" + data[i].productPrice + "</td>"
				isiHome += "<td>" + data[i].productStock + "</td>"
				isiHome += "<td class='text-center'><input type='checkbox' name='isactive' checked disabled></td>"
				isiHome += "<td class='text-right><button class='btn btn-warning' onclick='openModalEdit(" + data[i].id + ")'><i class='bi-pencil-square'></td>"
				isiHome += "<td class='text-right><button class='btn btn-danger' onclick='openModalDelete(" + data[i].id + ")'><i class='bi-trash'></td>"
				isiHome += "</tr>"
			}
			isiHome += "</tbody>"
			isiHome += "<table>"

			$('#isidata').html(isiHome)
		}
	})
}
