$(function() {
	getAllCategory()
})
function getAllCategory() {
	$.ajax({
		url: '/api/category',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = "<table border='1' class='table bg-light table-hover'>"
			str += "<thead>"
			str += "<tr>"
			str += "<th class='text-center'>Initial</th>"
			str += "<th class='text-center'>Name</th>"
			str += "<th class='text-center'>Active</th>"
			str += "<th colspan='2'  class='text-center'>Action</th>"
			str += "</th>"
			str += "</thead>"
			str += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				str += "<tr>"
				str += "<td>" + data[i].categoryInitial + "</td>"
				str += "<td>" + data[i].categoryName + "</td>"
				str += "<td class='text-center'><input type='checkbox' name='isactive' checked disabled ></td>"
				str += "<td class='text-right'><button class='btn btn-warning' onclick='openModalEdit(" + data[i].id + ") '><i class='bi-pencil-square'></button></td>"
				str += "<td class='text-left'><button class='btn btn-danger' onclick='openModalDelete(" + data[i].id + ") '><i class='bi-trash'></button></td>"
				str += "</tr>"

			}
			str += "</tbody>"
			str += "</table>"

			$('#isidata').html(str)
		}
	})
}

function openModal() {
	/*
	var str = '<form>'
	str += "<div class='form-group'>"
	str += "<label>Category Initial</label>"
	str += "<input type='text' class='form-control' id='categoryInitial'>"
	str += "</div>"
	str += "<div class='form-group'>"
	str += "<label>Category Name</label>"
	str += "<input type='text' class='form-control' id='categoryName'>"
	str += "</div>"
	str += "<div class='form-group form-check'>"
	str += "<input type='checkbox' class='form-check-input' id='isActive'>"
	str += "<label class='form-check label'>Active</label>"
	str += "</div>"
	str += "</form>"
	*/

	var str = "<table>"
	str += "<tr>"
	str += "<td width=75px><b>Initial</b></td>"
	str += "<td><input type='text' id='categoryInitial'></td>"
	str += "</tr>"
	str += "<tr>"
	str += "<td><b>Name</b></td>"
	str += "<td><input type='text' id='categoryName'></td>"
	str += "</tr>"
	str += "<tr>"
	str += "<td><b>Active</b></td>"
	str += "<td><input type='checkbox' id='isActive'></td>"
	str += "</tr>"
	str += "</table>"

	$('.modal-title').html('Create New')
	$('.modal-body').html(str)
	$('#btn-save').off('click').on('click', saveCategory).html('Create').removeClass('btn-success').addClass('btn-success')
	$('#modal').modal('show')

}

function saveCategory() {

	var initial = $('#categoryInitial').val()
	var name = $('#categoryName').val()
	var active = $('#isActive')[0].checked

	var formdata = '{'
	formdata += '"categoryInitial" : "' + initial + '",'
	formdata += '"categoryName" : "' + name + '",'
	formdata += '"isActive" : ' + active + ''
	formdata += '}'

	$.ajax({
		url: '/api/category/add',
		type: 'post',
		contentType: 'application/json',
		data: formdata,
		success: function() {
			$('#modal').modal('toggle')
			//$('.modal-body').html('')
			getAllCategory()
		}
	})
}

function openModalEdit(id2) {
	$.ajax({
		url: '/api/category/' + id2,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			/*
			var str = "<form>"
			str += "<div class='form-group'>"
			str += "<input type='hidden' name='id2' id='idCategory' value='" + id2 + "'>"
			str += "<label>Category Initial</label>"
			str += "<input type='text' class='form-control' id='categoryInitial' value='" + data.categoryInitial + "'>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Category Name</label>"
			str += "<input type='text' class='form-control' id='categoryName' value='" + data.categoryName + "'>"
			str += "</div>"
			str += "<div class='form-group form-check'>"
			str += "<input type='checkbox' class='form-check-input' id='isActive' checked>"
			str += "<label class='form-check label'>Active</label>"
			str += "</div>"
			str += "</form>"
			*/

			var str = "<table>"
			str += "<tr>"
			str += "<input type='hidden' name='id2' id='idCategory' value='" + id2 + "'>"
			str += "<td width=75px><b>Initial</b></td>"
			str += "<td><input type='text' id='categoryInitial' value='" + data.categoryInitial + "'></td>"
			str += "</tr>"
			str += "<tr>"
			str += "<td><b>Name</b></td>"
			str += "<td><input type='text' id='categoryName' value='" + data.categoryName + "'></td>"
			str += "</tr>"
			str += "<tr>"
			str += "<td><b>Active</b></td>"
			str += "<td><input type='checkbox' id='isActive' checked></td>"
			str += "</tr>"
			str += "</table>"

			$('.modal-title').html('Edit')
			$('.modal-body').html(str)
			$('#btn-save').off('click').on('click', editCategory).html('Save Change').removeClass('btn-warning').addClass('btn-warning')
			$('#modal').modal('show')
		}
	})
}

function editCategory() {
	var id2 = $('#idCategory').val()
	var initial2 = $('#categoryInitial').val()
	var name2 = $('#categoryName').val()
	var active2 = $('#isActive')[0].checked

	var formdata = '{'
	formdata += '"categoryInitial" : "' + initial2 + '",'
	formdata += '"categoryName" : "' + name2 + '",'
	formdata += '"isActive" : ' + active2 + ''
	formdata += '}'

	$.ajax({
		url: '/api/edit/category/' + id2,
		type: 'put',
		contentType: 'application/json',
		data: formdata,
		success: function() {
			$('#modal').modal('toggle')
			//$('.modal-body').html('')
			getAllCategory()
		}
	})
}

function openModalDelete(id3) {
	$.ajax({
		url: '/api/category/' + id3,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			/*
			var str = "<form>"
			str += "<div class='form-group'>"
			str += "<input type='hidden' name='id3' id='idCategory' value='" + id3 + "'>"
			str += "<label>Category Initial</label>"
			str += "<input type='text' class='form-control' id='categoryInitial' value='" + data.categoryInitial + "' disabled>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Category Name</label>"
			str += "<input type='text' class='form-control' id='categoryName' value='" + data.categoryName + "' disabled>"
			str += "</div>"
			str += "<div class='form-group form-check'>"
			str += "<input type='checkbox' class='form-check-input' id='isActive' checked disabled>"
			str += "<label class='form-check label'>Active</label>"
			str += "</div>"
			str += "</form>"
			*/

			var str = "<h5>Are you sure you want to delete this?</h5>"
			str += "<table align='center'>"
			str += "<tr>"
			str += "<input type='hidden' name='id3' id='idCategory' value='" + id3 + "'>"
			str += "<td width=75px><b>Initial</b></td>"
			str += "<td>" + data.categoryInitial + "</td>"
			str += "</tr>"
			str += "<tr>"
			str += "<td><b>Name</b></td>"
			str += "<td>" + data.categoryName + "</td>"
			str += "</tr>"
			str += "<tr>"
			str += "<td><b>Active</b></td>"
			str += "<td><input type='checkbox' id='isActive' checked disabled></td>"
			str += "</tr>"
			str += "</table>"

			$('.modal-title').html('Delete')
			$('.modal-body').html(str)
			$('#btn-save').off('click').on('click', deleteCategory).html('Delete').removeClass('btn-danger').addClass('btn-danger')
			$('#modal').modal('show')
		}
	})
}

function deleteCategory() {
	var id3 = $('#idCategory').val()
	var initial = $('#categoryInitial').val()
	var name = $('#categoryName').val()
	var active = $('#isActive')[0].checked

	var formdata = '{'
	formdata += '"categoryInitial" : "' + initial + '",'
	formdata += '"categoryName" : "' + name + '",'
	formdata += '"isActive" : ' + active + ''
	formdata += '}'

	$.ajax({
		url: '/api/delete/category/' + id3,
		type: 'put',
		contentType: 'application/json',
		data: formdata,
		success: function() {
			$('#modal').modal('toggle')
			//$('.modal-body').html('')
			getAllCategory()
		}
	})
}

function search() {
	$.ajax({
		url: '/api/categorysearch/' + $('#input-search').val(),
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = "<table border='1' class='table bg-light table-hover'>"
			str += "<thead>"
			str += "<tr>"
			str += "<th class='text-center'>Initial</th>"
			str += "<th class='text-center'>Name</th>"
			str += "<th class='text-center'>Active</th>"
			str += "<th colspan='2'  class='text-center'>Action</th>"
			str += "</th>"
			str += "</thead>"
			str += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				str += "<tr>"
				str += "<td>" + data[i].categoryInitial + "</td>"
				str += "<td>" + data[i].categoryName + "</td>"
				str += "<td class='text-center'><input type='checkbox' name='isactive' checked disabled ></td>"
				str += "<td class='text-right'><button class='btn btn-warning' onclick='openModalEdit(" + data[i].id + ") '><i class='bi-pencil-square'></button></td>"
				str += "<td class='text-left'><button class='btn btn-danger' onclick='openModalDelete(" + data[i].id + ") '><i class='bi-trash'></button></td>"
				str += "</tr>"

			}
			str += "</tbody>"
			str += "</table>"

			$('#isidata').html(str)
		}
	})
}
